.PHONY: all lint pretty clean

all: test

check: lint test radon bandit

# We use both becase they find fixes after each other
pretty: autopep8 yapf
	
venv: requirements.txt
	python3 -m venv venv
	. venv/bin/activate && pip3 install -r requirements.txt

lint:
	flake8

test: venv
	. venv/bin/activate && \
	coverage run --branch --omit="venv/*,tests/*,tests.py" tests.py && \
	coverage report && \
	coverage html --fail-under=70 && \
	coverage erase

autopep8:
	# TODO: add venv/* and migrations/* exclusion
	autopep8 --in-place --aggressive --aggressive -r .

yapf:
	# TODO: -e "venv/*|migrations/*" not working
	yapf -ir -e "venv/*|migrations/*" .

radon:
	radon cc -s -a --ignore='venv,migrations' .
	radon mi -s --ignore='venv,migrations' .

bandit:
	bandit -r --exclude='./venv/,./migrations/' .

clean:
	rm -rf venv
	rm -rf htmlcov
	find -iname "*.pyc" -delete
	find -type d -name __pycache__ -delete
