# Running application
Tested on Win10 with Visual Studio Code running in Windows Subsystem for Linux (requires no hardware virtualization)
If hardware virtualization is available, Vagrant is better choice.
## Install requirements  
```
# Install pip
sudo apt-get install python3-pip
# Install requirements
sudo pip3 install -r requirements.txt
# Create ablemic table in db (on first run only)
flask db init
# Create migration file (if we changed model)
flask db migrate
# Apply migration to database
flask db upgrade
# Run tests (Hint: They will fail. You need to find error and write missing method)
python3 tests.py
# Run app
flask run
```

## Telegraf 
```
# Run telegraf
./telegraf/usr/bin/telegraf --config telegraf.conf
# Run flask
flask run --host=0.0.0.0 --port=5003 >> flask.log 2>&1 & echo $! > flask.pid
# Check endpoints_stats
http://<hostname>:5003/endpoints_stats
```

If you stuck, my solution is in done branch. Feel free to open issues with questions.